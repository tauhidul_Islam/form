import {Component} from '@angular/core';
import {FormGroup, FormBuilder, FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  myForm: FormGroup;

  constructor(private _formBuilder: FormBuilder) {
    this.myForm = this._formBuilder.group({
      tuhin: null,
      world: null
    });
  }

  onSubmit() {
    if (this.myForm.valid) {
        // save data
        const formData = this.myForm.value;
        console.log(formData);
    } else {
        this.validateAllFields(this.myForm); 
    }
}

validateAllFields(formGroup: FormGroup) {     
    console.log('form invalid');    
    // Object.keys(formGroup.controls).forEach(field => {  
    //     const control = formGroup.get(field);            
    //     if (control instanceof FormControl) {             
    //         control.markAsTouched({ onlySelf: true });
    //     } else if (control instanceof FormGroup) {        
    //         this.validateAllFields(control);  
    //     }
    // });
}


someF(formValues,formValues2){
    console.log(formValues.value.f1 + '  ' + formValues2.value.f1);
}
}
